CREATE TABLE dth_activation_rpt AS
SELECT a.subscr_no, 
	a.SUBSCR_NO_RESETS,
   a.ACCOUNT_NO, 
   a.CREATE_WHO,
   CAST (NULL AS VARCHAR2(50)) market_type,
   CAST (NULL AS VARCHAR2(50)) customer_tagging,
   CAST (NULL AS VARCHAR2(50)) account_category,
   CAST (NULL AS VARCHAR2(50)) customer_id,
   CAST (NULL AS VARCHAR2(1000)) billing_address,
   CAST (NULL AS VARCHAR2(84)) bill_region,
   CAST (NULL AS DATE) ACTIVATION_DATE,
   CAST (NULL AS VARCHAR2(144))astro_order_no,
   CAST (NULL AS VARCHAR2(144)) astro_account_id,
   CAST (NULL AS VARCHAR2(144)) modem_id,
   CAST (NULL AS VARCHAR2(240)) Subscription_plan   
     FROM ord_service_order a, ord_item b, mxs_reporting_parameters c
    WHERE     A.SERVICE_ORDER_ID = b.SERVICE_ORDER_ID
          AND a.SERVICE_ORDER_TYPE_ID IN (10, 90)
          AND b.MEMBER_TYPE = 80
          and b.MEMBER_ID = c.INT1
          AND C.APPLICATION_NAME = 'Maxis_DTH_Emg_Config_ID'
          AND a.ORDER_STATUS_ID = 80
          AND a.COMPLETE_DT BETWEEN TO_DATE('&3','DDMMYYYY') and TO_DATE('&4','DDMMYYYY')+1.00001;

CREATE INDEX DTH_ACTIVATION_I ON dth_activation_rpt(SUBSCR_NO, SUBSCR_NO_RESETS);

CREATE TABLE dth_deactivation_rpt AS
   SELECT
   a.subscr_no, 
   a.SUBSCR_NO_RESETS,
   a.ACCOUNT_NO, 
   a.CREATE_WHO,
   CAST (NULL AS VARCHAR2(50)) market_type,
   CAST (NULL AS VARCHAR2(50)) customer_tagging,
   CAST (NULL AS VARCHAR2(50)) account_category,
   CAST (NULL AS VARCHAR2(50)) customer_id,
   CAST (NULL AS VARCHAR2(1000)) billing_address,
   CAST (NULL AS VARCHAR2(84)) bill_region,
   CAST (NULL AS DATE) ACTIVATION_DATE,
   CAST (NULL AS VARCHAR2(144))astro_order_no,
   CAST (NULL AS VARCHAR2(144)) astro_account_id,
   CAST (NULL AS VARCHAR2(144)) modem_id,
   CAST (NULL AS VARCHAR2(240)) Subscription_plan   
     FROM ord_service_order a, ord_item b, mxs_reporting_parameters c
    WHERE     A.SERVICE_ORDER_ID = b.SERVICE_ORDER_ID
          AND a.SERVICE_ORDER_TYPE_ID IN (40, 50, 100)
          AND b.MEMBER_TYPE = 80
          AND b.MEMBER_ID = c.INT1
          AND C.APPLICATION_NAME = 'Maxis_DTH_Emg_Config_ID'
          AND a.ORDER_STATUS_ID = 80
	  AND a.COMPLETE_DT BETWEEN TO_DATE('&3','DDMMYYYY') and TO_DATE('&4','DDMMYYYY')+1.00001;

CREATE INDEX DTH_DEACTIVATION_I ON dth_deactivation_rpt(SUBSCR_NO, SUBSCR_NO_RESETS);

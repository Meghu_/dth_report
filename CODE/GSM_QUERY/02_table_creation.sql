update dth_activation_rpt a set 
a.ACCOUNT_CATEGORY =
(select acv.display_value from account_category_values acv, cmf where acv.account_category = cmf.account_category and cmf.account_no= a.ACCOUNT_NO),
a.market_type = 
(select mkv.display_value from mkt_code_values mkv, cmf where mkv.MKT_CODE = cmf.MKT_CODE and cmf.account_no= a.ACCOUNT_NO);

update dth_activation_rpt a set  a.CUSTOMER_TAGGING =
(select gev.display_value from cmf_ext_data ced, GENERIC_ENUMERATION_VALUES gev, ENUMERATION_DEF ed, param_def pd
where ced.account_no = a.account_no 
and ced.param_value= gev.VALUE
and ed.ENUMERATION_ID = pd.ASSOC_ENUMERATION_ID
and PD.PARAM_ID =20
and PD.PARAM_ID = ced.param_id
and gev.ENUMERATION_KEY = ed.ENUMERATION_KEY);

update dth_activation_rpt a set a.customer_id = 
(select ciam.external_id from customer_id_acct_map ciam where ciam.account_no = a.ACCOUNT_NO 
and ciam.inactive_date is null and ciam.external_id_type =9);

update dth_activation_rpt a set a.customer_id = 
(select ciam.external_id from customer_id_acct_map ciam where ciam.account_no = a.ACCOUNT_NO 
and ciam.inactive_date is null and ciam.external_id_type =11)
where a.customer_id is null;

update dth_activation_rpt a set a.billing_address =
(select (cmf.BILL_ADDRESS1 || cmf.BILL_ADDRESS2 || cmf.BILL_ADDRESS3) from cmf where cmf.account_no = a.ACCOUNT_NO);

update dth_activation_rpt a set BILL_REGION =
(select NVL(cmf.BILL_STATE, cmf.cust_state) from cmf where cmf.account_no = a.ACCOUNT_NO);

update dth_activation_rpt a set a.ACTIVATION_DATE =
(SELECT b.SERVICE_ACTIVE_DT FROM SERVICE b WHERE a.subscr_no = b.subscr_no and b.SUBSCR_NO_RESETS = a.SUBSCR_NO_RESETS and b.SERVICE_INACTIVE_DT is null);

update dth_activation_rpt a 
set a.ASTRO_ORDER_NO =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS, ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc nulls first) as myrank 
from customer_id_equip_map ciem, dth_activation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =125
and ciem.IS_CURRENT = 1) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.SUBSCR_NO_RESETS= c.SUBSCR_NO_RESETS);

update dth_activation_rpt a 
set a.ASTRO_ACCOUNT_ID =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS,ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc nulls first) as myrank 
from customer_id_equip_map ciem, dth_activation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =124
and ciem.IS_CURRENT = 1) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.SUBSCR_NO_RESETS= c.SUBSCR_NO_RESETS);

update dth_activation_rpt a 
set a.MODEM_ID =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS,ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc nulls first) as myrank 
from customer_id_equip_map ciem, dth_activation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =126
and ciem.IS_CURRENT = 1) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.subscr_no_resets= c.subscr_no_resets);

update dth_activation_rpt a 
set a.Subscription_plan =
(select PACKAGE_DESC from
(select srp.parent_subscr_no,srp.PARENT_SUBSCR_NO_RESETS,srp.PACKAGE_DESC, DENSE_RANK() OVER (partition by srp.parent_subscr_no, srp.PARENT_SUBSCR_NO_RESETS order by srp.INACTIVE_DT desc nulls first) as myrank 
from Mxsrpt_sub_rate_plan srp, dth_activation_rpt b
where srp.parent_subscr_no = b.SUBSCR_NO) c
where c.myrank =1
and a.subscr_no = c.parent_subscr_no
and a.subscr_no_resets= c.PARENT_SUBSCR_NO_RESETS);

commit;



update dth_deactivation_rpt a set 
a.ACCOUNT_CATEGORY =
(select acv.display_value from account_category_values acv, cmf where acv.account_category = cmf.account_category and cmf.account_no= a.ACCOUNT_NO),
a.market_type = 
(select mkv.display_value from mkt_code_values mkv, cmf where mkv.MKT_CODE = cmf.MKT_CODE and cmf.account_no= a.ACCOUNT_NO);

update dth_deactivation_rpt a set  a.CUSTOMER_TAGGING =
(select gev.display_value from cmf_ext_data ced, GENERIC_ENUMERATION_VALUES gev, ENUMERATION_DEF ed , param_def pd
where ced.account_no = a.account_no 
and ced.param_value= gev.VALUE
and ed.ENUMERATION_ID = pd.ASSOC_ENUMERATION_ID
and PD.PARAM_ID =20
and PD.PARAM_ID = ced.param_id
and gev.ENUMERATION_KEY = ed.ENUMERATION_KEY);

update dth_deactivation_rpt a 
set a.customer_id =
(select EXTERNAL_ID from
(select ciam.external_id,ciam.ACCOUNT_NO, DENSE_RANK() OVER (partition by ciam.ACCOUNT_NO order by ciam.INACTIVE_DATE desc nulls first) as myrank 
from customer_id_acct_map ciam, dth_deactivation_rpt b
where ciam.account_no = b.account_no 
and ciam.external_id_type =9) c
where c.myrank =1
and a.ACCOUNT_NO = c.ACCOUNT_NO);

update dth_deactivation_rpt a 
set a.customer_id =
(select EXTERNAL_ID from
(select ciam.external_id,ciam.ACCOUNT_NO, DENSE_RANK() OVER (partition by ciam.ACCOUNT_NO order by ciam.INACTIVE_DATE desc nulls first) as myrank 
from customer_id_acct_map ciam, dth_deactivation_rpt b
where ciam.account_no = b.account_no 
and B.CUSTOMER_ID is null
and ciam.external_id_type =11) c
where c.myrank =1
and a.ACCOUNT_NO = c.ACCOUNT_NO
and a.customer_id is null);

update dth_deactivation_rpt a set a.billing_address =
(select (cmf.BILL_ADDRESS1 || cmf.BILL_ADDRESS2 || cmf.BILL_ADDRESS3) from cmf where cmf.account_no = a.ACCOUNT_NO);

update dth_deactivation_rpt a set BILL_REGION =
(select NVL(cmf.BILL_STATE, cmf.cust_state) from cmf where cmf.account_no = a.ACCOUNT_NO);

update dth_deactivation_rpt a set a.ACTIVATION_DATE =
(SELECT b.SERVICE_ACTIVE_DT FROM SERVICE b WHERE a.subscr_no = b.subscr_no and b.SUBSCR_NO_RESETS = a.SUBSCR_NO_RESETS);

update dth_deactivation_rpt a 
set a.ASTRO_ORDER_NO =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS, ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc) as myrank 
from customer_id_equip_map ciem, dth_deactivation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =125 
and ciem.INACTIVE_DATE is not null) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.SUBSCR_NO_RESETS= c.SUBSCR_NO_RESETS);

update dth_deactivation_rpt a 
set a.ASTRO_ACCOUNT_ID =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS,ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc) as myrank 
from customer_id_equip_map ciem, dth_deactivation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =124 
and ciem.INACTIVE_DATE is not null) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.SUBSCR_NO_RESETS= c.SUBSCR_NO_RESETS);

update dth_deactivation_rpt a 
set a.MODEM_ID =
(select EXTERNAL_ID from
(select ciem.subscr_no,ciem.SUBSCR_NO_RESETS,ciem.external_id, DENSE_RANK() OVER (partition by ciem.subscr_no, ciem.SUBSCR_NO_RESETS order by ciem.INACTIVE_DATE desc) as myrank 
from customer_id_equip_map ciem, dth_deactivation_rpt b
where ciem.subscr_no = b.SUBSCR_NO 
and ciem.external_id_type =126
and ciem.INACTIVE_DATE is not null) c
where c.myrank =1
and a.subscr_no = c.subscr_no
and a.subscr_no_resets= c.subscr_no_resets);

update dth_deactivation_rpt a 
set a.Subscription_plan =
(select PACKAGE_DESC from
(select srp.parent_subscr_no,srp.PARENT_SUBSCR_NO_RESETS,srp.PACKAGE_DESC, DENSE_RANK() OVER (partition by srp.parent_subscr_no, srp.PARENT_SUBSCR_NO_RESETS order by srp.INACTIVE_DT desc) as myrank 
from Mxsrpt_sub_rate_plan srp, dth_deactivation_rpt b
where srp.parent_subscr_no = b.SUBSCR_NO 
and srp.INACTIVE_DT is not null) c
where c.myrank =1
and a.subscr_no = c.parent_subscr_no
and a.subscr_no_resets= c.PARENT_SUBSCR_NO_RESETS);

commit;

declare

cursor cur_table_list is
    select table_name
    from
        user_tables
    where
        upper(table_name) in (
                 'DTH_ACTIVATION_RPT_UNION', 'DTH_DEACTIVATION_RPT_UNION'
        );

begin

    for rec_table_name in cur_table_list
    loop
        begin
            execute immediate 'drop table '||rec_table_name.table_name;
        end;
    end loop;

end;
/

declare

cursor cur_index_list is
    select index_name
    from
        USER_INDEXES
    where
        upper(INDEX_NAME) in (
                 'DTH_ACTIVATION_UNION_I', 'DTH_DEACTIVATION_UNION_I' 
	);
begin

    for rec_index_name in cur_index_list
    loop
        begin
            execute immediate 'drop index '||rec_index_name.index_name;
        end;
    end loop;

end;
/
#!/usr/bin/sh
#***************************************************************************************
# Maxis Telecom
# ******************	Version : 1.0 **************************************************
# 
# Execution Syntax: nohup mxs_report_wrapper.sh &
# Example:nohup mxs_report_wrapper.sh &
#****************************************************************************************

##----- Report Main Configuration File ----------------------------
#. /rptmig/DTH_REPORT/CODE/init.ini
#. /home/RPTDUMP/repdev/DTH_REPORT/CODE/init.ini
. /apps/arbor/custom/DTH_REPORT/CODE/init.ini

##----- Program main variables section ----------------------------
pdate=`date +"%Y%m%d%H%M%S"`
main_logfile=$report_log/mxs_report_wrapper_${pdate}.log
pdate_1=`date +"%s"`
s_date=`date +"%d-%m-%Y %H:%M:%S"`


##*********Checking whether the current or related process already in progress or not*************

check_runp=`ps -ef | grep mxs_report_wrapper.sh | grep -v vi |  grep -v grep | wc -l`
sleep 2
if [ ${check_runp} -gt 1  ]; then
        echo "Process mxs_report_wrapper.sh is already in progress" >> $main_logfile
        exit 1
fi
#****************************************************************************************
echo " archiving the logs"
#****************************************************************************************
# ARCHIVE PREVIOUS LOG AND REPORT FILES
#****************************************************************************************
i=0
while [[ $i -lt  ${#scan_locations[@]} ]]
do
    for file_extension in ${file_extensions[@]}
    do
        # Moving previous log files
        if [ -f ${scan_locations[$i]}/*.${file_extension} ]
        then
            if [ ${file_zip} = "TRUE" ] ; then
                gzip ${scan_locations[$i]}/*.${file_extension}
            fi
            
            if [ ${file_move} = "TRUE"  ] ; then
                mv ${scan_locations[$i]}/*.${file_extension}* ${bkp_locations[$i]}
            fi
        fi
    done

   ((i=i+1))
done
#echo " archiving completed here"
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Script Started" >> $main_logfile
echo "Script Started at : $s_date" >> $main_logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Backup Existing Log and Report Files" >> $main_logfile

#******************************************CONNECTION FOR SQL************************************#
#define SQL for connect string

echo " Connecting with the SQL"

SQL_CONNECT=`echo $ORACLE_HOME/bin/sqlplus -s/nolog $auth_1/$auth_2@$main_inh_db`
export SQL_CONNECT
TRIM_HEAD=`echo "head -4"`
export TRIM_HEAD
TRIM_TAIL=`echo "tail -1"`
export TRIM_TAIL

echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Validate Input Parameters" >> $main_logfile
#****************************************************************************************
# CHECK INPUT PARAMETERS
#****************************************************************************************

if [ $# -eq 0 ]; then

	START_DT=`echo "SELECT TO_CHAR(SYSDATE-1,'DDMMYYYY') FROM DUAL;"`
	START_DT=`((echo $START_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export START_DT
	echo "START_DT: $START_DT"

	END_DT=`echo "SELECT TO_CHAR(SYSDATE-1,'DDMMYYYY') FROM DUAL;"`
	END_DT=`((echo $END_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export END_DT
	echo "END_DT: $END_DT"

elif [ $# -eq 1  ] ; then

        START_DT=`echo "SELECT TO_CHAR(TO_DATE('$1','DDMMYYYY'),'DDMMYYYY') FROM DUAL;"`
	START_DT=`((echo $START_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export START_DT
	echo "START_DT: $START_DT"

	END_DT=`echo "SELECT TO_CHAR(TO_DATE('$1','DDMMYYYY'),'DDMMYYYY') FROM DUAL;"`
	END_DT=`((echo $END_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL  `
	echo "END_DT : $END_DT "
        export END_DT

elif [ $# -eq 2  ] ; then

        START_DT=`echo "SELECT TO_CHAR(TO_DATE('$1','DDMMYYYY'),'DDMMYYYY') FROM DUAL;"`
	START_DT=`((echo $START_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL`
	export START_DT
	echo "START_DT: $START_DT"

	END_DT=`echo "SELECT TO_CHAR(TO_DATE('$2','DDMMYYYY'),'DDMMYYYY') FROM DUAL;"`
	END_DT=`((echo $END_DT | $SQL_CONNECT) | $TRIM_HEAD) | $TRIM_TAIL  `
	echo "END_DT : $END_DT "
        export END_DT

elif [ $# -gt 2  ] ; then

	echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Invalid number of arguments passed" >> $main_logfile
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Terminated" >> $main_logfile

    echo "Invalid Parameter | Usage : wrapper.sh"
	exit 1
fi
#**************************Obtaining GSMs Names******************************************
server_num=5
for db_conn in ${main_db_links[@]}
do
    db_links[$server_num]=`echo $db_conn`
    gsm_links_process[${server_num}-5]=$!
    ((server_num=server_num+1))
done

wait_process 'batch_gsm_links' $main_logfile ${gsm_links_process[@]}

#****************************************************************************************
#****************************************************************************************
# GSM PROCESSING
#****************************************************************************************

echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS |  GSM DB PROCESSING " >> $main_logfile
server_num=5
for db_conn in ${main_db_names[@]}
do
    . $report_code/mxs_mig_batch_exe.sh GSM_QUERY $db_conn $main_inh_db $START_DT $END_DT &    

gsm_query_process[${server_num}-5]=$!
((server_num=server_num+1))
done
wait_process 'batch_query_gsm' $main_logfile ${gsm_query_process[@]}


#******************************INHOUSE PROCESSING FOR RESULT CHECK***********************************************

echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | DATA UNION ON INHOUSE DB" >> $main_logfile
 
 . $report_code/mxs_mig_batch_exe.sh RESULT_CHECK $main_inh_db $START_DT $END_DT ${db_links[5]} ${db_links[6]} ${db_links[7]} ${db_links[8]} ${db_links[9]} &

result_check_process=$!
wait_process 'result_check_mxpinh' $main_logfile ${result_check_process}


#****************************** EXTRACTION  **********************************************


echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Extracting Output Reports from MXPINH" >> $main_logfile
. $report_code/mxs_mig_extraction_exe.sh EXTRACTION $main_inh_db $START_DT $END_DT &

report_extract_process=$!
wait_process 'report_extraction' $main_logfile ${report_extract_process}

exit 0;
#******************************* sftp section ********************************************

echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Loader sftp transfer file started" >> $main_logfile
chmod -R 777 $report_data/*.txt
sftp $SFTP_TARGET_IP<<EOT
lcd $report_data
cd $STP_TARGET_DIR
pwd
mput *.txt
sftp_status=$?
chmod 777 *.txt
bye
exit
EOT

if [[ $sftp_status -eq 0 ]] then
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Loader file transfer completed" >> $main_logfile
else
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Error during sftp of encyypted loader file." >> $main_logfile
	exit 1
fi

# Program Completed

e_time=`date +"%d-%m-%Y %H:%M:%S"`
pdate_2=`date +"%s"`
time_diff=`expr $pdate_2 - $pdate_1`
echo "Script Completed at : $e_time" >> $main_logfile
echo "Script took $time_diff seconds to get complete." >> $main_logfile 
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Completed" >> $main_logfile
#***************************************************************************
# END OF EXTRACTION PROGRAM
#***************************************************************************

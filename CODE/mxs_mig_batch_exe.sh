#!/usr/bin/sh
#***************************************************************************************
# Maxis Telecom
# Filename: mxs_mig_batch_exe.sh
# Created By: Seemansh Gangwar
# Created On: 15 Sept 2019
# ******************	Version : 1.0 **************************************************
# 
# Execution Syntax: nohup mxs_mig_batch_exe.sh sql_folder db_name db_link &
# Example:nohup mxs_mig_batch_exe.sh PROJECT_GREEN_ENT FXPGSM1 FXPGSM1P &
#****************************************************************************************

##----- Report Main Configuration File ----------------------------

#. /rptmig/DTH_REPORT/CODE/init.ini
#. /home/RPTDUMP/repdev/DTH_REPORT/CODE/init.ini
. /apps/arbor/custom/DTH_REPORT/CODE/init.ini

##----- Program main variables section ----------------------------
pdate=`date +"%Y%m%d%H%M%S"`
logfile=$report_log/mxs_mig_batch_exe_${1}_${pdate}.log

echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Script Started" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | STEPS | Validate Input Parameters " >> $logfile
#****************************************************************************************
# CHECK INPUT PARAMETERS
 echo " Total number of parameters  :  $#"
 echo " $1 $2 $3 $4 $5 $6 $7 $8 $9"
#****************************************************************************************
if [  $# -eq 1  ] ; then

    sql_folder=$1
    db_name=$main_db_names
    
    sql_list=${report_code}/list_batch_${sql_folder}.temp

elif [  $# -eq 2  ] ; then

	sql_folder=$1
    db_name=$2
    
    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}.temp
    
elif [  $# -eq 3  ] ; then

	sql_folder=$1
    db_name=$2
    db_link=$3
    
    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}_${db_link}.temp

elif [  $# -eq 9  ] ; then

    sql_folder=$1
    db_name=$2
    db_link=$2
    start_date=$3
    end_date=$4
    db_link1=$5
    db_link2=$6
    db_link3=$7
    db_link4=$8
    db_link5=$9

    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}.temp

elif [  $# -eq 5  ] ; then

    sql_folder=$1
    db_name=$2
    db_link=$3
    start_date=$4
    end_date=$5       
    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}_${db_link}.temp	
    
elif [  $# -eq 7  ] ; then

    sql_folder=$1
    db_name=$2
    db_link=$3
    date_1=$4
    time_1=$5
    date_2=$6
    time_2=$7
    
    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}_${db_link}.temp

elif [  $# -eq 8  ] ; then

    sql_folder=$1
    db_name=$2
    db_link=$3
    date_1=$4
    time_1=$5
    date_2=$6
    time_2=$7
    bill_cycle=$8
    
    sql_list=${report_code}/list_batch_${sql_folder}_${db_name}_${db_link}_${bill_cycle}.temp

else

	echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | Invalid number of arguments passed" >> $logfile
	echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Terminated" >> $logfile

    echo "Invalid Parameter | Usage : mxs_mig_batch_exe.ksh <SQL Folder> <db_name> [db_link] [start_date] [end_date]"
	exit 1
fi
#echo " Batch Process Called with folder     : $sql_folder "
#echo " Batch Process Called with DB         : $db_name "
#echo " Batch Process Called with Link       : $db_link "
 
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | SQL Folder : $sql_folder" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | DB Name : $db_name" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | DB Link : $db_link" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | START DATE : $start_date">> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | END DATE : $end_date">> $logfile

#exit 0

##-------------------- HERE THE PROGRAM RUNS NOW --------------------------------##
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | Maxis Batch Execution Program - Start" >> $logfile
#****************************************************************************************
# Validate if the BATCH SQL Folder and the SQL File Exists
#****************************************************************************************
if [ -d ${report_sql}/${sql_folder} ] ; then

    ls -1 ${report_sql}/${sql_folder} | grep .sql > ${sql_list}
    ## Check if any SQL script in the folder
    count_list_tx=`wc -l ${sql_list} | awk '{ print$1 }'`
    if [ $count_list_tx -eq 0 ]; then
        echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | There is no SQL file found in ${report_sql}/${sql_folder} folder" >> $logfile

        exit 1
    fi

else 

    echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | There is no SQL file found in ${report_sql}/${sql_folder} folder" >> $logfile
    echo "${sql_folder} folder is NOT found in ${report_sql} directory!!!"

    exit 1
fi


#****************************************************************************************
# Batch Process Function
#****************************************************************************************
function batch_process
{

    db=${1}
    
    db_logfile=$report_log/batch_exe_${sql_folder}_${db}_${db_link}_${pdate}.log
    
    while read script_name
    do

        echo "`date +"%Y-%m-%d.%H:%M:%S"` | START TIME | SQL Execution $script_name in $db Started" >> $db_logfile

        batch_err_msg=`sqlplus -s /nolog << EOF
            connect ${auth_1}/${auth_2}@${db}

            @${report_sql}/${sql_folder}/${script_name} $auth_1 $db_link $start_date $end_date $db_link1 $db_link2 $db_link3 $db_link4 $db_link5
        exit;
        EOF
        `

        #****************************************************************************************
        # ERROR HANDLING SECTION
        #****************************************************************************************
        out=`echo $batch_err_msg | egrep -e 'ORA|SP2|CPY|ERROR'`
        if [ $? -eq 0 ]; then

            echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | SQL ERROR - SQL File Execution: \n $batch_err_msg" >> $db_logfile
            echo "`date +"%Y-%m-%d.%H:%M:%S"` | ERROR | SQL ERROR - ${db} - SQL File Execution: ${script_name}" >> $logfile
            
            exit 2
        fi

        echo "`date +"%Y-%m-%d.%H:%M:%S"` | END TIME | SQL Execution $script_name in $db Completed" >> $db_logfile

    done < ${sql_list}
    
    echo "`date +"%Y-%m-%d.%H:%M:%S"` | INFO | ALL SQL EXECUTION in ${db} Completed" >> $logfile
}



echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS START | SQL Script Batch Execution Started" >> $logfile
#****************************************************************************************
# SQL EXECUTION
#****************************************************************************************
i=0
for db_conn in ${db_name[@]}
do
    batch_process $db_conn &
    
    batch_process_list[${i}]=$!
    ((i=i+1))
done

wait_process 'batch_process' $logfile ${batch_process_list[@]}


# End Looping for SQL Execution on each DB
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | SQL Script Batch Execution Completed" >> $logfile

rm -r ${sql_list}

# Program Completed
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Maxis Batch Execution Program - Completed" >> $logfile
echo "`date +"%Y-%m-%d.%H:%M:%S"` | PROCESS END | Script Completed" >> $logfile
#***************************************************************************
# END OF BATCH TRANSFORMATION
#***************************************************************************
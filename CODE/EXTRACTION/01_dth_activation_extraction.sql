SELECT DISTINCT
  SUBSCR_NO
  ||','
  || ACCOUNT_NO
  ||','
  || REPLACE(REPLACE(REPLACE(REPLACE(CREATE_WHO,chr(13),''),chr(10),''),chr(34),''),',','')
  ||','
  || MARKET_TYPE
  ||','
  || CUSTOMER_TAGGING
  ||','
  || ACCOUNT_CATEGORY
  ||','
  || CUSTOMER_ID
  ||','
  || REPLACE(REPLACE(REPLACE(REPLACE(BILLING_ADDRESS,chr(13),''),chr(10),''),chr(34),''),',','')
  ||','
  || REPLACE(REPLACE(REPLACE(REPLACE(BILL_REGION,chr(13),''),chr(10),''),chr(34),''),',','')
  ||','
  || ACTIVATION_DATE
  ||','
  || ASTRO_ORDER_NO
  ||','
  || ASTRO_ACCOUNT_ID
  ||','
  || MODEM_ID
  ||','
  ||SUBSCRIPTION_PLAN
FROM DTH_ACTIVATION_RPT_UNION;